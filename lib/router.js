'use strict';

var controller = require('../controller'),
    express = require('express');

var router = new express.Router();

router.get('/', function (req, res) {
        res.send(controller.getAllArticles());
    }).
    get('/:id', function (req, res) {
        res.send(controller.getArticle(req.params.id));
    });

module.exports = router;
