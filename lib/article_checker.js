'use strict';

var _ = require('underscore'),
    articleCache = require('./article_cache'),
    diffText = require('diff-text');

 var findDuplicateArticle = exports.findDuplicateArticle = function (article) {
    var diff;

    return _.find(articleCache.getArticlesByFeedId(article.feedId), function (cachedArticle) {
        diff = diffText(article.title, cachedArticle.title);
        if (diff.similarPercent > 50) {
            return true;
        }
    });
};

var findSimilarArticle = exports.findSimilarArticle = function (article) {
    var diff;

    return _.find(articleCache.getArticles(), function (cachedArticle) {
        if (article.feedId !== cachedArticle.feedId) {
            diff = diffText(article.title, cachedArticle.title);
            if (diff.similarPercent > 50) {
                return true;
            } else {
                diff = diffText(article.summary || article.content, cachedArticle.summary || cachedArticle.content);
                if (diff.similarPercent > 50) {
                    return true;
                }
            }
        }
    });
};

exports.checkArticle = function (article) {
    var duplicateArticle;

    duplicateArticle = findDuplicateArticle(article);

    if (duplicateArticle) {
        return {
            duplicate: true,
            duplicateArticle: duplicateArticle
        };
    }

    duplicateArticle = findSimilarArticle(article);

    if (duplicateArticle) {
        return {
            similar: true,
            duplicateArticle: duplicateArticle
        };
    }

    return {
        newArticle: true
    };
};
