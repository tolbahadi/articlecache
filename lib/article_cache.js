var _ = require('underscore');

exports.CACHE_LIMIT = 40;

var articleCache = exports.__CACHED_ARTICLES = [];

exports.clearCache = function  () {
    articleCache.length = 0;
};

exports.pushArticle = function  (article) {
    articleCache.unshift(article);

    if (articleCache.length > exports.CACHE_LIMIT) {
        articleCache.pop();
    }
    return article;
};

exports.removeArticle = function (linkOrArticle) {
    var articleIndex;
    if (_.isString(linkOrArticle)) {
        _.find(articleCache, function (article, index) {
            if (article.link === linkOrArticle) {
                articleIndex = index;
                return true;
            }
        });
    } else {
        articleIndex = _.indexOf(articleCache, linkOrArticle);
    }

    if (articleIndex != null) {
        articleCache.splice(articleIndex, 1);
    }
};

exports.getArticle = function (link) {
    return _.find(articleCache, function (article) {
        return article.link === link;
    });
};

exports.getArticles = function () {
    return articleCache;
};

exports.getArticlesByFeedId = function (feedId) {
    return _.filter(articleCache, function (article) {
        return article.feedId === feedId;
    });
};
