'use strict';

require('should');
var _ = require('underscore');
var articleCache = require('../lib/article_cache');

describe('ArticleCache', function () {
    it('#exists', function () {
        articleCache.should.be.ok;
    });

    describe('#CACHE_LIMIT', function () {
        it('#defaults to 40', function () {
            articleCache.CACHE_LIMIT.should.eql(40);
        });
    });

    describe('#__CACHED_ARTICLES', function () {
        it('#is an array', function () {
            articleCache.__CACHED_ARTICLES.should.an.Array;
        });
    });

    describe('#clearCache', function () {
        it('#exists', function () {
            articleCache.clearCache.should.a.Function;
        });
        it('#clears the cache', function () {
            articleCache.__CACHED_ARTICLES.push('stuff');
            articleCache.clearCache();
            articleCache.__CACHED_ARTICLES.should.eql([]);
        });
    });

    describe('#pushArticle', function () {
        it('#exists', function () {
            articleCache.pushArticle.should.be.ok;
            articleCache.pushArticle.should.be.a.Function;
        });
        it('#actually pushes things', function () {
            articleCache.clearCache();
            var a = {link: 'link1'};
            articleCache.pushArticle(a);
            articleCache.__CACHED_ARTICLES[0].should.eql(a);
        });
        it('#actually pushes things to the front', function () {
            articleCache.clearCache();
            var a1 = {link: 'link1'};
            var a2 = {link: 'link2'};
            articleCache.pushArticle(a1);
            articleCache.pushArticle(a2);
            articleCache.__CACHED_ARTICLES[0].should.eql(a2);
            articleCache.__CACHED_ARTICLES[1].should.eql(a1);
        });
        it('#respect limit', function () {
            articleCache.clearCache();
            articleCache.CACHE_LIMIT = 2;
            articleCache.pushArticle(1);
            articleCache.pushArticle(2);
            articleCache.__CACHED_ARTICLES.length.should.eql(2);
            articleCache.pushArticle(3);
            articleCache.__CACHED_ARTICLES.length.should.eql(2);
            articleCache.__CACHED_ARTICLES[0].should.eql(3);
            articleCache.CACHE_LIMIT = 40;
        });
    });

    describe('#removeArticle', function () {
        it('#exists', function () {
            articleCache.removeArticle.should.be.ok;
            articleCache.removeArticle.should.be.a.Function;
        });
        it('#passed a link', function () {
            articleCache.clearCache();
            articleCache.pushArticle({link: '1'});
            articleCache.pushArticle({link: '2'});
            articleCache.pushArticle({link: '3'});
            articleCache.removeArticle('2');
            articleCache.__CACHED_ARTICLES.length.should.eql(2);
            _.indexOf(articleCache.__CACHED_ARTICLES, {link: '2'}).should.eql(-1);
        });
        it('#passed object', function () {
            articleCache.clearCache();
            articleCache.pushArticle({link: '1'});
            var a = {link: '2'};
            articleCache.pushArticle(a);
            articleCache.pushArticle({link: '3'});
            articleCache.removeArticle(a);
            articleCache.__CACHED_ARTICLES.length.should.eql(2);
            _.indexOf(articleCache.__CACHED_ARTICLES, a).should.eql(-1);
        });
    });

    describe('#getArticle', function () {
        it('#exists', function () {
            articleCache.getArticle.should.be.ok;
            articleCache.getArticle.should.be.a.Function;
        });
        it('#workds', function () {
            articleCache.clearCache();
            articleCache.pushArticle({link: '1'});
            articleCache.pushArticle({link: '2'});
            articleCache.pushArticle({link: '3'});
            articleCache.getArticle('3').should.eql({link: '3'});
        });
    });

    describe('#getArticles', function () {
        it('#exists', function () {
            articleCache.getArticles.should.be.ok;
            articleCache.getArticles.should.be.a.Function;
        });
        it('#workds', function () {
            articleCache.clearCache();
            articleCache.pushArticle({link: '1'});
            articleCache.pushArticle({link: '2'});
            articleCache.pushArticle({link: '3'});
            articleCache.getArticles().should.eql(articleCache.__CACHED_ARTICLES);
        });
    });

    describe('#getArticlesByFeedId', function () {
        it('#exists', function () {
            articleCache.getArticlesByFeedId.should.be.ok;
            articleCache.getArticlesByFeedId.should.be.a.Function;
        });
        it('#workds', function () {
            articleCache.clearCache();
            articleCache.pushArticle({link: '1', feedId: 1});
            articleCache.pushArticle({link: '2', feedId: 2});
            articleCache.pushArticle({link: '3', feedId: 1});
            articleCache.getArticlesByFeedId(1).should.eql([
                {link: '3', feedId: 1}, {link: '1', feedId: 1}
            ]);
        });
    });

    
});
