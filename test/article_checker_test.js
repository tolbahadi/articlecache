'use strict';

require('should');
var articleChecker = require('../lib/article_checker'),
    articleCache = require('../lib/article_cache');

describe('ArticleChecker', function () {
    it('#defined', function () {
        articleChecker.should.be.ok;
    });

    it('#duplicate articles', function () {
        articleCache.clearCache();
        articleCache.pushArticle({title: 'one two tree', feedId: 1});
        articleCache.pushArticle({title: 'five six two four', feedId: 2});

        var duplicate = articleChecker.findDuplicateArticle({title: 'one two five', feedId: 1}, articleCache);

        duplicate.should.be.ok;
        duplicate.should.eql({title: 'one two tree', feedId: 1});
    });

    it('#similar articles', function () {
        articleCache.clearCache();
        articleCache.pushArticle({title: 'one two tree', feedId: 1});
        articleCache.pushArticle({title: 'five six two four', feedId: 2});
        articleCache.pushArticle({title: 'five one two six four three', feedId: 2});

        var duplicate = articleChecker.findSimilarArticle({title: 'one two five', feedId: 1}, articleCache);

        duplicate.should.be.ok;
        duplicate.should.eql({title: 'five one two six four three', feedId: 2});
    });

    it('#checkArticle', function () {
        articleCache.clearCache();
        articleCache.pushArticle({title: 'one two tree', feedId: 1});
        articleCache.pushArticle({title: 'five six two four', feedId: 2});

        var result = articleChecker.checkArticle({title: 'one two five', feedId: 1}, articleCache);

        result.should.be.ok;
        result.duplicate.should.be.true;
        result.duplicateArticle.should.eql({title: 'one two tree', feedId: 1});
    });

    it('#similar articles', function () {
        articleCache.clearCache();
        articleCache.pushArticle({title: 'one two tree', feedId: 1});
        articleCache.pushArticle({title: 'five six two four', feedId: 2});
        articleCache.pushArticle({title: 'five one two six four three', feedId: 2});

        var result = articleChecker.checkArticle({title: 'five one two five', feedId: 5}, articleCache);

        result.should.be.ok;
        result.similar.should.be.true;
        result.duplicateArticle.should.eql({title: 'five one two six four three', feedId: 2});
    });
});
