# article_cache [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-url]][daviddm-image]

Cache recent articles

## Getting Started
Install the module with: `npm install article-cache`

```javascript
var article-cache = require('article-cache');
article-cache.awesome(); // "awesome"
```

## Documentation
_(Coming soon)_

## Examples
_(Coming soon)_

## Contributing
In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [gulp](http://gulpjs.com/).

## Release History
_(Nothing yet)_

## License
Copyright (c) 2014 . Licensed under the MIT license.

[npm-url]: https://npmjs.org/package/article-cache
[npm-image]: https://badge.fury.io/js/article-cache.png
[travis-url]: https://travis-ci.org//article-cache
[travis-image]: https://travis-ci.org//article-cache.png?branch=master
[daviddm-url]: https://david-dm.org//article-cache.png?theme=shields.io
[daviddm-image]: https://david-dm.org//article-cache
