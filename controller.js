'use strict';

var articleCache = require('./lib/article_cache'),
    articleChecker = require('./lib/article_checker'),
    _ = require('underscore');

/**
 * An article is
 * {
 *     link: string, unique
 *     title: string
 *     summary: string
 *     content: string
 * }
 */

module.exports = {
    setCacheLimit: function (limit) {
        articleCache.CACHE_LIMIT = limit;
    },
    addArticle: function (article) {
        if (_.isEmpty(article) || !article.link) {
            throw 'Invalid article! ' + JSON.stringify(article);
        } else if (articleCache.getArticle(article.link)) {
            throw 'Article with the same link exists';
        }
        return articleCache.pushArticle(article);
    },
    removeArticle: function (link) {
        var article = articleCache.getArticle(link);
        if (!article) {
            throw 'Article not found!';
        } else {
            articleCache.removeArticle(article);
        }
    },
    getArticle: function (link) {
        return articleCache.getArticle(link);
    },
    findDuplicateArticle: function (article) {
        return articleChecker.findDuplicateArticle(article);
    },
    findSimilarArticle: function (article) {
        return articleChecker.findSimilarArticle(article);
    },
    getAllArticles: function () {
        return articleCache.getArticles();
    },
    getFeedArticles: function (feedId) {
        return articleCache.getArticlesByFeedId(feedId);
    }
};
